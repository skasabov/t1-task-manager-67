package ru.t1.skasabov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.skasabov.tm.api.ProjectEndpoint;
import ru.t1.skasabov.tm.dto.ProjectDto;
import ru.t1.skasabov.tm.repository.ProjectDtoRepository;
import ru.t1.skasabov.tm.repository.TaskDtoRepository;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.t1.skasabov.tm.api.ProjectEndpoint")
public final class ProjectEndpointImpl implements ProjectEndpoint {

    @Autowired
    private ProjectDtoRepository projectRepository;

    @Autowired
    private TaskDtoRepository taskRepository;

    @NotNull
    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<ProjectDto> findAll() {
        return projectRepository.findAll();
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/save")
    public ProjectDto save(
            @NotNull
            @WebParam(name = "project", partName = "project")
            @RequestBody final ProjectDto project
    ) {
        return projectRepository.save(project);
    }

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public ProjectDto findById(
            @NotNull
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        return projectRepository.findById(id).orElse(null);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @NotNull
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        return projectRepository.existsById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return projectRepository.count();
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @NotNull
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        taskRepository.deleteByProjectId(id);
        projectRepository.deleteById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @NotNull
            @WebParam(name = "project", partName = "project")
            @RequestBody final ProjectDto project
    ) {
        taskRepository.deleteByProjectId(project.getId());
        projectRepository.delete(project);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(
            @NotNull
            @WebParam(name = "projects", partName = "projects")
            @RequestBody final List<ProjectDto> projects
    ) {
        for (@NotNull final ProjectDto project : projects) taskRepository.deleteByProjectId(project.getId());
        projectRepository.deleteAll(projects);
    }

    @Override
    @WebMethod
    @PostMapping("/clear")
    public void clear() {
        @NotNull final List<ProjectDto> projects = projectRepository.findAll();
        for (@NotNull final ProjectDto project : projects) taskRepository.deleteByProjectId(project.getId());
        projectRepository.deleteAll();
    }

}
