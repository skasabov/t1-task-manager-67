package ru.t1.skasabov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.skasabov.tm.api.TaskEndpoint;
import ru.t1.skasabov.tm.dto.TaskDto;
import ru.t1.skasabov.tm.repository.TaskDtoRepository;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.t1.skasabov.tm.api.TaskEndpoint")
public final class TaskEndpointImpl implements TaskEndpoint {

    @Autowired
    private TaskDtoRepository taskRepository;

    @NotNull
    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<TaskDto> findAll() {
        return taskRepository.findAll();
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/save")
    public TaskDto save(
            @NotNull
            @WebParam(name = "task", partName = "task")
            @RequestBody final TaskDto task
    ) {
        return taskRepository.save(task);
    }

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public TaskDto findById(
            @NotNull
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        return taskRepository.findById(id).orElse(null);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @NotNull
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        return taskRepository.existsById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return taskRepository.count();
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @NotNull
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        taskRepository.deleteById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @NotNull
            @WebParam(name = "task", partName = "task")
            @RequestBody final TaskDto task
    ) {
        taskRepository.delete(task);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(
            @NotNull
            @WebParam(name = "tasks", partName = "tasks")
            @RequestBody final List<TaskDto> tasks
    ) {
        taskRepository.deleteAll(tasks);
    }

    @Override
    @WebMethod
    @PostMapping("/clear")
    public void clear() {
        taskRepository.deleteAll();
    }

}
