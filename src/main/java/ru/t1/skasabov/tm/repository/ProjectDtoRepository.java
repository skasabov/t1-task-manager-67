package ru.t1.skasabov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1.skasabov.tm.dto.ProjectDto;

public interface ProjectDtoRepository extends JpaRepository<ProjectDto, String> {

}
